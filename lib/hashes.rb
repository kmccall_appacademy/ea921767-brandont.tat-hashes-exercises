# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str_hash = {}

  str.split.each do |word|
    str_hash[word] = word.length
  end

  str_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{|k, v| v}.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_freq = Hash.new(0)

  word.chars.each do |c|
    letter_freq[c] += 1
  end

  letter_freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq = Hash.new(0)
  arr.each {|el| uniq[el] += 1}
  uniq.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odds = Hash.new(0)

  numbers.each do |num|
    num.even? ? even_odds[:even] += 1 : even_odds[:odd] += 1
  end

  even_odds
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  vowel_hash = Hash.new(0)

  vowels.each_char do |c|
    vowel_hash[c] = string.count(c)
  end

  vowel_hash.sort_by{|k, v| v}.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students = students.select{|name, month| month >= 7}
  students.keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = {}

  specimens.uniq.each do |specimen|
    species[specimen] = specimens.count(specimen)
  end

  number_of_species = species.length
  smallest_population_size = species.values.min
  largest_population_size = species.values.max

  number_of_species**2 * (smallest_population_size/largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counts = character_count(normal_sign)
  vandalized_counts = character_count(vandalized_sign)

  vandalized_counts.keys.all? do |char|
    vandalized_counts[char] <= normal_counts[char]
  end
end

def character_count(str)
  letter_counts = {}

  str.downcase.delete(' ').chars.uniq do |char|
    letter_counts[char] = str.downcase.count(char)
  end

  letter_counts
end
